package programs;

import java.util.*;
import java.text.DecimalFormat;

public class Assignmentemployee {

		    double calavg(int[] age)
		    {
		        int len=age.length;
		        double sum=0.0;
		        for(int i=0;i<len;i++)
		        {
		            sum+=age[i];
		        }
		        double avg=sum/len;
		        return avg;
		    }
		    public static void main (String[] args) {
		    	
		        Scanner s =new Scanner(System.in);
		        Assignmentemployee obj=new Assignmentemployee();
		        System.out.println("Enter the total number of employees:");
		        int n=s.nextInt();
		        int flag=0;
		        if(n>1)
		        { int[] age=new int[n];
		            System.out.println("Enter the age for "+n+" employees:");
		            for(int i=0;i<n;i++)
		            {
		                int temp=s.nextInt();
		                if(temp>=28 && temp<=40)
		                {
		                    age[i]=temp;
		                }
		                else
		                {
		                    System.out.println("Invalid age");
		                    flag++;
		                    break;
		                }
		            }
		            if(flag==0)
		            {   DecimalFormat df=new DecimalFormat("####.00");
		                System.out.println("The average age is "+df.format(obj.calavg(age)));
		            }
		        }
		        else
		        {
		            System.out.println("Please enter the correct count of employee");
		        }
		    }
		
	

}
