package programs;

import java.util.ArrayList;
import java.util.Iterator;

public class Assignmentarraylist {

	public static void main(String[] args) {

		ArrayList<Integer>elements=new ArrayList<>();	
		elements.add(20);
		elements.add(10);
		elements.add(40);
		System.out.println(" elements are : " + elements);
		System.out.println(" size of array: " + elements.size());
			
		// while loop
		System.out.println(" 1. while loop elements: ");
		 
		 Iterator<Integer> itrtr = elements.iterator();
		 while(itrtr.hasNext())
		
		{
			System.out.println(itrtr.next());
		}
		 
		 // advanced for loop
		 
		 System.out.println(" \n2. advanced for loop elements: ");
		 
		 for(Object o : elements )
		 {
			 System.out.println(o);
		 }
		 
		// for loop
		 
		 System.out.println(" \n3. for loop: ");
		 
		 for(int i=0;i<elements.size();i++)
		 {
			 System.out.println(elements.get(i));
		 }
		
	}

}
